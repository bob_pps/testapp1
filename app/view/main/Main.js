Ext.define('TestApp.view.main.Main', {
    extend  : 'Ext.Container',
    alias   : 'widget.main',
    xtype   : 'main',

    controller  : 'main',
    viewModel   : {
        type: 'main'
    },

    requires: [
        'TestApp.view.main.MainModel',
        'TestApp.view.main.MainController'
    ],

    layout: {
        type    : 'hbox',
        align   : 'stretch'
    },

    flex: 1,

    defaults: {
        border: true,
        margin: 10
    },
    items: [
        {
            xtype   : 'grid',
            flex    : 1.5,
            title   : 'Grid',
            columns : {
                defaults: {
                    flex: 1
                },
                items: [
                    { text: 'ID', dataIndex: 'id', flex: .5 },
                    { text: 'Name', dataIndex: 'name' },
                    { text: 'Title', dataIndex: 'title' },
                    { text: 'Description', dataIndex: 'description' },
                    { text: 'Date', dataIndex: 'date', formatter: 'date("d.m.Y")' }
                ]
            },
            tbar: [
                {
                    text    : 'Add',
                    handler : 'addRecord'
                },
                {
                    text    : 'Delete',
                    handler : 'deleteRecord',
                    disabled: true,
                    bind: {
                        disabled: '{isNewOrNotExist}'
                    }
                }
            ],
            bind: {
                store       : '{testStore}',
                selection   : '{record}'
            }

        },
        {
            xtype   : 'panel',
            flex    : 1,
            title   : 'Form',
            items   : [
                {
                    xtype       : 'form',
                    layout      : 'anchor',
                    hidden      : true,
                    defaultType : 'textfield',
                    padding     : '0 20',
                    bodyPadding : '20 0',
                    defaults: {
                        anchor: '100%'
                    },
                    bind: {
                        hidden: '{!record}'
                    },
                    tbar: [
                        '->',
                        {
                            text    : 'Save',
                            handler : 'saveRecord'
                        },
                        {
                            text    : 'Reject',
                            handler : 'rejectRecord'
                        }
                    ],
                    items: [
                        {
                            fieldLabel: 'ID',
                            bind: {
                                value   : '{record.id}',
                                disabled: '{!isNewOrNotExist}'
                            }
                        },
                        {
                            fieldLabel  : 'Name',
                            bind        : '{record.name}',
                            allowBlank  : false
                        },
                        {
                            fieldLabel  : 'Title',
                            bind        : '{record.title}'
                        },
                        {
                            xtype       : 'textarea',
                            fieldLabel  : 'Description',
                            bind        : '{record.description}'
                        },
                        {
                            xtype       : 'datefield',
                            fieldLabel  : 'Date',
                            bind        : '{record.date}',
                            maxWidth    : 240
                        }
                    ]
                }
            ]
        }
    ]
});