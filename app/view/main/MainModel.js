Ext.define('TestApp.view.main.MainModel', {
    extend  : 'Ext.app.ViewModel',
    alias   : 'viewmodel.main',

    requires: [
        'TestApp.model.Test'
    ],

    data: {
        record: null
    },

    formulas: {
        isNewOrNotExist: function(get){
            var record = get('record');
            return !record || record.phantom;
        }
    },

    stores: {
        testStore: {
            autoLoad: true,
            model   : 'TestApp.model.Test',

            proxy: {
                type: 'ajax',
                url : '/resources/data/testdata.json'
            }
        }
    }
});