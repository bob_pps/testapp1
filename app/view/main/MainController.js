Ext.define('TestApp.view.main.MainController', {
    extend  : 'Ext.app.ViewController',
    alias   : 'controller.main',

    privates: {
        addRecord: function(){
            var me = this,
                vm = me.getViewModel(),
                view = me.getView(),
                grid = view.down('grid'),
                form = view.down('form');

            grid.getSelectionModel().deselectAll();

            vm.set('record', Ext.create('TestApp.model.Test', {
                date: new Date()
            }));

            setTimeout(function(){
                form.getForm().clearInvalid();
            }, 10);
        },

        deleteRecord: function(){
            var me = this,
                vm = me.getViewModel();

            vm.get('record').erase();
        },

        saveRecord: function(){
            var me = this,
                vm = me.getViewModel(),
                isNew = vm.get('isNewOrNotExist'),
                store = vm.get('testStore'),
                record = vm.get('record'),
                grid = me.getView().down('grid'),
                form = me.getView().down('form');

            if(!form.isValid()){
                return;
            }

            if(isNew){
                if(!Ext.isNumeric(record.get('id'))){
                    record.set('id', store.max('id') + 1);
                }
                store.add(record);
            }

            record.save();
        },

        rejectRecord: function(){
            var me = this,
                vm = me.getViewModel(),
                isNew = vm.get('isNewOrNotExist');

            if(isNew){
                vm.set('record', null);
            }
            else{
                vm.get('record').reject();
            }
        }
    }
});