Ext.define('TestApp.model.Test', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'title', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s' }
    ],

    proxy: {
        type: 'memory'
    }
});